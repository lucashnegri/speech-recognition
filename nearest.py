# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 10:42:57 2013
@author: Lucas Hermann Negri
"""

from sklearn.neighbors import KNeighborsClassifier
from base import *

# classificadores

knn1 = {
    'nome': 'KNN - k=3, uniform',
    'impl': KNeighborsClassifier(n_neighbors = 3, weights = 'uniform')
}

knn2 = {
    'nome': 'KNN - k=10, uniform',
    'impl': KNeighborsClassifier(n_neighbors = 10, weights = 'uniform')
}

knn3 = {
    'nome': 'KNN - k=20, uniform',
    'impl': KNeighborsClassifier(n_neighbors = 20, weights = 'uniform')
}

knn4 = {
    'nome': 'KNN - k=3, distance',
    'impl': KNeighborsClassifier(n_neighbors = 3, weights = 'distance')
}

knn5 = {
    'nome': 'KNN - k=10, distance',
    'impl': KNeighborsClassifier(n_neighbors = 10, weights = 'distance')
}

knn6 = {
    'nome': 'KNN - k=20, distance',
    'impl': KNeighborsClassifier(n_neighbors = 20, weights = 'distance')
}

classificadores = [knn1, knn4]
executa(dados, classificadores)

