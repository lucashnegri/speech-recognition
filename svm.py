# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 17:58:45 2013
@author: Lucas Hermann Negri
"""

from sklearn.svm     import LinearSVC
from sklearn.svm     import SVC
from base import *

linear = {
    'nome': 'LinearSVC',
    'impl': LinearSVC(dual = False, C = 0.2)
}

gaussian = {
    'nome': 'SVC (rbf)',
    'impl': SVC(C = 50) 
}

classificadores = [linear, gaussian]
executa(dados, classificadores)
