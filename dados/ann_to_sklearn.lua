#! /usr/bin/env lua5.1

local inputs  = {"../0-Original/mod_digitos", "../0-Original/mod_completo"}
local names   = {"digitos", "completo"}
local classes = {10, 26}

for i, input in ipairs(inputs) do
    local inp = io.open(input)
    local m = inp:read('*n')
    local n = inp:read('*n')
    
    local out_x = io.open(names[i] .. '_x', 'w')
    local out_y = io.open(names[i] .. '_y', 'w')
    
    for a = 1, m do
        for b = 1, n - classes[i] do
            out_x:write( inp:read('*n'), ' ' )
        end
        out_x:write('\n')
        
        local idx_h, val_h = -1, -1
        for b = 1, classes[i] do
            local val = inp:read('*n')
            if val > val_h then
                val_h = val
                idx_h = b
            end
        end
        
        out_y:write(idx_h, '\n')
    end
    
    inp:close()
    out_x:close()
    out_y:close()
end
