# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 11:24:58 2013
@author: Lucas Hermann Negri
"""

from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from base import *

rf1 = {
    'nome': 'RandomForestClassifier n = 100',
    'impl': RandomForestClassifier(n_estimators = 100, n_jobs=4)
}

rf2 = {
    'nome': 'RandomForestClassifier n = 300',
    'impl': RandomForestClassifier(n_estimators = 300, n_jobs=4)
}

et1 = {
    'nome': 'ExtraTrees',
    'impl': ExtraTreesClassifier(n_estimators = 300, n_jobs=4)
}

et2 = {
    'nome': 'ExtraTrees',
    'impl': ExtraTreesClassifier(n_estimators = 1000, n_jobs=4)
}

classificadores = [et2]
executa(dados, classificadores)
