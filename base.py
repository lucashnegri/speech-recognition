# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 10:00:33 2013

@author: kknd
"""

from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score
from sklearn.metrics          import accuracy_score
from sklearn.decomposition    import RandomizedPCA
from numpy                    import genfromtxt, savetxt

def reduz(X, n):
    pca = RandomizedPCA(n_components = n)
    return pca.fit_transform(X)

def naive(svm, X, y):
    svm.fit(X, y)
    R = svm.predict(X)
    return "%.6f" % ( accuracy_score(y, R, normalize=True) )

def kfold(svm, X, y, k):
    kf = KFold(n = X.shape[0], n_folds=k)
    scores = cross_val_score(svm, X, y, cv = kf)    
    return "%.6f +/- %.6f" % ( scores.mean(), scores.std() )

def executa(dados, classificadores):
    for cls in classificadores:
        print "\n*** Classificador: %s ***" % cls['nome']
    
        for dado in dados:
            print "\n** Dado: %s **" % dado['nome']
            print "\n* naive *"
            print naive(cls['impl'], dado['X'], dado['y'])
            
            print "\n* 10-fold *"
            print kfold(cls['impl'], dado['X'], dado['y'], 10)

# carregamento dos dados
digitos = {
    'nome': 'Digitos',
    'X'   : genfromtxt('dados/digitos_x'),
    'y'   : genfromtxt('dados/digitos_y')
}

completo = {
    'nome': 'Completo',
    'X'   : genfromtxt('dados/completo_x'),
    'y'   : genfromtxt('dados/completo_y')
}

# reduz a dimensionalidade
n_comp = 80
digitos ['X'] = reduz(digitos['X'], n_comp)
completo['X'] = reduz(completo['X'], n_comp)
#savetxt('digitos_pca', digitos['X'])
#savetxt('completo_pca', completo['X'])

dados = [digitos, completo]
