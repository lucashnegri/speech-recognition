# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 11:19:55 2013
@author: Lucas Hermann Negris
"""

from sklearn.naive_bayes import GaussianNB
from base import *

gaussian = {
    'nome': 'GaussianNB',
    'impl': GaussianNB()
}

classificadores = [gaussian]
executa(dados, classificadores)