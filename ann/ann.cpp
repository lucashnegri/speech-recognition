#include <supernn>
#include <cstdio>
#include <algorithm>
#include <armadillo>

using namespace SuperNN;

int main(int argc, char* argv[])
{
    Utils::rand_seed();
    
    Network net = Network::make_mlp(80, 75, 26);
    net.set_activation(ACT_SIGMOID_SYMMETRIC, 0.5);
    
    IRprop t;
    Data full;
    
    full.load_file("mod_completo_pca");
    full.calc_bounds();
    full.scale(-1, 1);
    random_shuffle( full.begin(), full.end() );
    
    int k_folds = 10;

    arma::running_stat<double> stats;

    // cross-validation by k-folds
    for(int i = 0; i < k_folds; ++i)
    {
        Data train, test;
        
        printf("** Training ANN %d of %d...**\n", i+1, k_folds);
        full.k_fold(i, k_folds, test, train);
        
        net.init_weights(-0.2, 0.2);
        t.prepare(net);
        
        for(int p = 0; p < 100; ++p)
        {
            t.train(net, train, 0, 1);
            printf("Iteration %d (%f, %f)\n", p, net.calc_class_higher(train), net.calc_class_higher(test));
        }
        
        double out = net.calc_class_higher(test);
        printf("\rResult: %.6f\n", out);
        stats(out);
    }
    
    printf( "%.6f +/- %.6f\n", stats.mean(), stats.stddev() );
    
    return 0;
}
