#! /usr/bin/lua 5.1

local inpX   = assert( io.open(arg[1]) )
local inpY   = assert( io.open(arg[2]) )
local out    = assert( io.open(arg[3], 'w'))
local nclass = 26

while true do
    local l = inpX:read('*l')
    if not l then break end
    
    out:write(l)
    local c = inpY:read('*n')
    for i = 1, nclass do
        out:write( i == c and ' 1' or ' 0')
    end
    out:write('\n')
end

out:close()
inpX:close()
inpY:close()
